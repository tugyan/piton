from qtconsole.qt import QtGui, QtCore
from qtconsole.rich_jupyter_widget import RichJupyterWidget

class EnhancedJupyterWidget(RichJupyterWidget):
    ignore_list = ["module", "function", "ufunc", "built-in", "builtin_function_or_method", "type"]
    varFilterCmd = """__ignore_list = ["Out", "In", "quit", "exit", "get_ipython"]\n
__exclude_types = ['module', 'function', 'builtin_function_or_method',
'instance', '_Feature', 'type', 'ufunc']
__tmpVars = dir()
__P = [__var for __var in __tmpVars if not __var.startswith('_') and __var not in __ignore_list]\n
__P = [(__var, str(__val)) for __var,__val in vars().items() if __var in __P and type(__val).__name__ not in __exclude_types]\n
__P = [k if "ndarray" not in type(k[1]).__name__ else (k[0], 'array ' + str(k[1].shape)) for k in __P]
[(k[0], str(k[1])) if str(k[1]).startswith('<') else k for k in __P]
"""
    np_arr_rcv_trg = QtCore.pyqtSignal(str)
    flowState = "user"
    def __init__(self, *args, **kw):
        super(RichJupyterWidget, self).__init__(*args, **kw)
        self.currVal = ''
        self.nump_array_requested = False
        self.i = 0
        self.numpy_array_string = ""
        self.varModel = QtGui.QStandardItemModel()

    def get_numpy_array(self, name):
        self.flowState = "npArrReq"
        self.kernel_client.execute('{}.tolist()\n'.format(name), store_history=False)

    def _handle_execute_input(self, msg):
        if self.flowState == "user":
            self.flowState = "varsReq"
            self.kernel_client.execute("%whos", store_history=False)
            super()._handle_execute_input(msg)

        elif self.flowState == "varsReq":
            self.flowState = "updateVars"
        elif self.flowState == "npArrReq":
            pass
        else:
            self.flowState = "user"

    def _handle_stream(self, msg):
        if self.flowState == "updateVars":
            self._parse_and_add_vars(msg)
            self.flowState = "user"
        else:
            super(type(self), self)._handle_stream(msg)

    def _handle_error(self, msg):
        self.flowState = "user"
        super(type(self), self)._handle_error(msg)


    def _handle_execute_result(self, msg):
        if self.flowState == "npArrReq":
             self.nump_array_requested = False
             cvars = msg['content']['data']['text/plain']
             self.np_arr_rcv_trg.emit(cvars)
             self.flowState = "user"

        else:
            super()._handle_execute_result(msg)

    def add_var(self, var):
        varName = var[0]
        varValue = str(var[2])
        items = []
        items.append(QtGui.QStandardItem(varName))
        items.append(QtGui.QStandardItem(varValue))
        items[1].setData(var[1])
        items[0].setToolTip(var[3])
        items[1].setToolTip(var[3])
        self.varModel.appendRow(items)


    def _parse_and_add_vars(self, cvars):
        self.varModel.clear()
        self.varModel.setHorizontalHeaderLabels(["Name", "Value"])
        cvars = cvars['content']['text'].split("\n")
        cvars = [k.split("  ") for k in cvars]
        cvars = [[p for p in k if p != ""] for k in cvars[2:-1]]
        print(cvars)
        for k in cvars:
            if k[1].strip() in self.ignore_list:
                continue

            if k[1] == 'ndarray':
                arr_prop = k[2].split(",")
                k[2] = " ".join(arr_prop[0:2])
                k.append(k[1] + " " + arr_prop[-1])
            else:
                k.append(k[1])

            self.add_var(k)
