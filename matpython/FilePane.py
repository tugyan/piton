from PyQt5 import QtWidgets, QtCore
import os

class FilePane(QtWidgets.QFrame):
    request_execute = QtCore.pyqtSignal(list)
    def __init__(self, parent=None):
        super(FilePane, self).__init__(parent)
        self.layout = QtWidgets.QGridLayout()

        self.upButton = QtWidgets.QToolButton()
        self.upButton.setArrowType(QtCore.Qt.UpArrow)
        self.upButton.clicked.connect(self.goUpperDir)

        self.fileTree = QtWidgets.QTreeView()
        self.fileTree.doubleClicked.connect(self.fileAction)
        self.file_system_model = QtWidgets.QFileSystemModel()
        self.current_dir = os.getcwd()
        self.file_system_model.setRootPath(self.current_dir)
        self.fileTree.setModel(self.file_system_model)
        self.fileTree.setRootIndex(self.file_system_model.index(os.getcwd()))
        #  self.file_system_model.insertRow()


        self.layout.addWidget(self.upButton, 0, 0, 1, 1)
        self.layout.addWidget(self.fileTree, 1, 0, 4, 5)
        self.setLayout(self.layout)

    def goUpperDir(self):
        self.current_dir = os.path.dirname(self.current_dir)
        self.setDir()

    def fileAction(self, idx):
        clickedName = self.file_system_model.filePath(idx)
        isPDir = self.file_system_model.isDir(idx)

        if isPDir == True:
            self.current_dir = clickedName
            self.setDir()

        else:
            if clickedName.endswith('.py'):
                self.request_execute.emit(["%run -i '" + clickedName + "'", False])
            elif clickedName.endswith('.mat'):
                self.request_execute.emit(["from scipy.io import loadmat\nmat_dict = loadmat('" + clickedName + "')", False])


    def setDir(self):
        self.fileTree.setRootIndex(self.file_system_model.index(self.current_dir))
        self.request_execute.emit(["import os\nos.chdir('" + self.current_dir + "')", True])
